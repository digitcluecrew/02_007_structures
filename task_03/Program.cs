﻿using System;

namespace task_03
{
    class MyClass
    {
        public string change;
    }

    struct MyStruct
    {
        public string change;
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
            MyClass @class = new MyClass();
            MyStruct @struct = new MyStruct();

            @class.change = "not changed!";
            @struct.change = "not changed!";

            ClassTaker(@class);
            StruktTaker(@struct);

            Console.WriteLine("Class {0}", @class.change);
            Console.WriteLine("Struct {0}", @struct.change);
            Console.ReadKey();
        }

        static void ClassTaker(MyClass myClass)
        {
            myClass.change = "changed!";
        }

        static void StruktTaker(MyStruct myStruct)
        {
            myStruct.change = "changed!";
        }
    }
}
