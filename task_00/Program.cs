﻿using System;

namespace task_00
{
    struct Notebook
    {
        string model;
        string manufacturer;
        double price;

        public Notebook(string model, string manufacturer, double price)
        {
            this.model = model;
            this.manufacturer = manufacturer;
            this.price = price;
        }

        public void Show()
        {
            Console.WriteLine("Model: {0}", this.model);
            Console.WriteLine("Manufacturer: {0}", this.manufacturer);
            Console.WriteLine("Price: {0}", this.price);
        }
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
            Notebook note = new Notebook("trololo", "foo", 100500);

            note.Show();

            Console.ReadKey();
        }
    }
}
