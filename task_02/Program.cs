﻿using System;

namespace task_02
{
    struct Train
    {
        public string destination;
        public int number;
        public string destTime;

        public Train(int number, string dest, string destTime)
        {
            this.number = number;
            this.destination = dest;
            this.destTime = destTime;
        }
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
            Train[] trains = new Train[2];

            for (int i = 0; i < trains.Length; i++)
            {
                Console.WriteLine("Specify information about {0} train", i + 1);
                Console.Write("Destination: ");
                string destination = Console.ReadLine();
                Console.Write("Destination time: ");
                string destTime = Console.ReadLine();

                trains[i] = new Train(i, destination, destTime);
            }

            Console.WriteLine("What are you looking for?");

            int target = int.Parse(Console.ReadLine());

            if (target < 1 || target > trains.Length){
                Console.WriteLine("No such train!");
            } else {
                Console.WriteLine("Train index: {0}", target - 1);
                Console.WriteLine("Train destination: {0}", trains[target - 1].destination);
                Console.WriteLine("Train destination time: {0}", trains[target - 1].destTime);
            }

            Console.WriteLine(trains.Length);

            Console.ReadKey();
        }
    }
}
